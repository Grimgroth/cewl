#!/bin/bash

# prérequis
# sudo apt install cewl

echo "Usage : $0 URL/FIchierURLs"

Profondeur=0
MinLongueur=5

echo "Url / FichierURLs : $1"
echo "Profondeur : $Profondeur"
echo "MinLongueur : $MinLongueur"
echo "Outpufile : ./CarftedWordlist.txt"

if [ -e $1 ];
then
## Lecture du fichier
	echo "Le fichier $1 existe !";
	for line in $(cat $1);
	do
		echo "$line" ;

		echo "Recupperation $line"
		cewl -d $Profondeur -m $MinLongueur -w ./tmp_passwd1.txt $line

		echo "Suppression des doublons"
		sort -u ./tmp_passwd1.txt > ./tmp_passwd2.txt
	
		echo "Convertion en minuscule"
		tr '[:upper:]' '[:lower:]' < ./tmp_passwd2.txt >> ./tmp_passwd3.txt
		
	done
else

	echo "Recupperation $1"
	cewl -d $Profondeur -m $MinLongueur -w ./tmp_passwd1.txt $1

	echo "Suppression des doublons"
	sort -u ./tmp_passwd1.txt > ./tmp_passwd2.txt

	echo "Convertion en minuscule"
	tr '[:upper:]' '[:lower:]' < ./tmp_passwd2.txt >> ./tmp_passwd3.txt

fi

cat ./tmp_passwd3.txt | sort | uniq > ./tmp_passwd4.txt

## prérequis
## sudo apt  install john

echo "Construction du fichier"
john --wordlist=./tmp_passwd4.txt --rules --stdout > ./CarftedWordlist.txt

echo "Suppression fichiers de travail"
if [ -e ./passwd1.txt ]; then rm -f ./tmp_passwd1.txt; fi
if [ -e ./passwd2.txt ]; then rm -f ./tmp_passwd2.txt; fi
if [ -e ./passwd3.txt ]; then rm -f ./tmp_passwd3.txt; fi
if [ -e ./passwd4.txt ]; then rm -f ./tmp_passwd4.txt; fi

